

#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationToolMT.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationChain.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationBaseHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationL1CaloHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationFastCaloHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationFastElectronHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationFastPhotonHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationPrecisionCaloHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationPrecisionElectronHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationPrecisionPhotonHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationPrecisionTrackingHypoTool.h"

DECLARE_COMPONENT( Trig::TrigEgammaEmulationToolMT )

DECLARE_COMPONENT( Trig::TrigEgammaEmulationChain )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationBaseHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationL1CaloHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationFastCaloHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationFastElectronHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationFastPhotonHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationPrecisionCaloHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationPrecisionElectronHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationPrecisionPhotonHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationPrecisionTrackingHypoTool )


